## REACTAR - STORE

🔸 Tiny library to handle global states in React applications.

It is based on a simplified version of context. It uses subscribers to feed the value consumption hook and thus avoid re-renders in all the components of the tree. Compatible with React +18.

```bash
npm i @reactar/store
```

In order to use the library a store must be declared.

```javascript
import { CreateStore } from "@reactar/store";

export const { Store, useStore } = CreateStore({
  ...oneSlice(),
  ...twoSlice(),
});
```

Is necessary to declare the provider in order to use the consume hook.

```javascript
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <Store.Provider>
    <App />
  </Store.Provider>
);
```

It is also necessary to declare a slice to compose the store.

```javascript
export const oneSlice = () => {
  return {
    oneState: {
      name: "name",
      address: "address",
    },
  };
};
```

To use the value in the store, we simply need to import the constant declared when we created the store.

```javascript
const [value, setValue] = useStore();

console.log("this way we access all the value within the store", value);
```

We can also pass a selector, so that the store returns a nested value.

```javascript
const [value, setValue] = useStore((value) => value.oneState);

console.log("this way we only access the nested value inside oneState", value);
```

To update a value inside the store, we simply have to use the setter.

```javascript
const [value, setValue] = useStore((value) => value.oneState);

setValue({ name: "new name" });
```
