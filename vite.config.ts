import { defineConfig } from "vite";
import { resolve } from "path";
import react from "@vitejs/plugin-react";
import dts from "vite-plugin-dts";

export default defineConfig({
  server: {
    host: true,
    watch: {
      usePolling: true,
    },
    port: 1000,
    strictPort: true,
  },
  build: {
    lib: {
      entry: resolve(__dirname, "index.ts"),
      name: "store",
      formats: ["es", "umd"],
      fileName: (format) => `index.${format}.js`,
    },
    emptyOutDir: true,
    manifest: false,
    outDir: "./build",
    rollupOptions: {
      external: ["react", "react-dom", "react/jsx-runtime", /node_modules/],
      output: {
        sourcemap: false,
        assetFileNames: "index.[ext]",
        globals: {
          react: "React",
          "react-dom": "ReactDOM",
          "react/jsx-runtime": "jsxRuntime",
        },
      },
    },
    reportCompressedSize: false,
    chunkSizeWarningLimit: 500,
  },
  publicDir: false,
  clearScreen: false,
  plugins: [react(), dts()],
});
