import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useRef,
  useSyncExternalStore,
} from "react";

export default function CreateStore<Store>(initialState: Store) {
  type UseStoreData<Store> = {
    get: () => Store;
    set: (value: Partial<Store>) => void;
    sub: (callback: () => void) => () => void;
  };
  function useStoreData(): UseStoreData<Store> {
    const store = useRef(initialState);
    const subscribers = useRef(new Set<() => void>());
    const get = useCallback(() => {
      return store.current;
    }, []);
    const set = useCallback((value: Partial<Store>) => {
      store.current = { ...store.current, ...value };
      subscribers.current.forEach((callback) => {
        return callback();
      });
    }, []);
    const sub = useCallback((callback: () => void) => {
      subscribers.current.add(callback);
      return () => {
        subscribers.current.delete(callback);
      };
    }, []);

    return { get, set, sub };
  }

  type UseStoreReturnType = ReturnType<typeof useStoreData>;
  const StoreContext = createContext<UseStoreReturnType | null>(null);

  const Provider = ({ children }: { children: ReactNode }) => {
    const store = useStoreData();
    return (
      <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
    );
  };

  const Store = {
    Provider,
  };

  function useStore<SelectorOutput = Store>(
    selector?: (store: Store) => SelectorOutput
  ): [SelectorOutput | Store, (value: Partial<Store>) => void] {
    const store = useContext(StoreContext);
    if (!store) {
      throw new Error("Store not found. Please provide one.");
    }
    const subscribe = store.sub;
    const getSnapshot = () => {
      return selector ? selector(store.get()) : store.get();
    };
    const getServerSnapshot = () => {
      return selector ? selector(initialState) : initialState;
    };
    const state = useSyncExternalStore(
      subscribe,
      getSnapshot,
      getServerSnapshot
    );
    return [state, store.set];
  }

  return { Store, useStore };
}
